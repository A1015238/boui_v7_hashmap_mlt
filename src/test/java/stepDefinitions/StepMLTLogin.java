package stepDefinitions;

//import java.util.List;

import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.reusable.library.PageObjectO2;

import io.cucumber.java.en.And;

//import org.junit.runner.RunWith;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;


@RunWith(Cucumber.class)
public class StepMLTLogin{

	public static WebDriver driver;
	PageObjectO2 pgObj = new PageObjectO2();
	MLTComponents ObjMLTComp = new MLTComponents();
	public ExtentTest report;
	
	
	public String EmplId_RT;
	public String EventDate_RT;
	public String StartDateTemp;
	public String PayCompValFile;
	public String PayComponent_RT;
	public String PayCompVal_RT;
	

	@Given("^LoginPage$")
	public void loginpage() throws Throwable {

		System.out.println("Execution Control Enters into Given statement for LoginPage");
		System.out.println("Execution Completed for Given statement for LoginPage");

	}

	@When("^EnterUserName$")
	public void enterusername() throws Throwable {
		
		System.out.println("Execution Control Enters into When statement for LoginPage");
		System.out.println("BDD executed When statement for LoginPage");

	}

	@Then("^Details$")
	public void details() throws Throwable {
		System.out.println("BDD executed Then statement for LoginPage");
	}


	@Given("^User in the Home Page$")
    public void user_in_the_home_page() throws Throwable {
		
		System.out.println("Execution Control Enters into User in the Home Page Statement");
		ObjMLTComp.loginMLT();
		
		System.out.println("Execution Completed for User in the Home Page Statement");
    }

    @When("^Search the file for  (.+) with the (.+)$")
    public void search_the_file_for_with_the(String transaction, String instanceid) throws Throwable {
    	
    	Thread.sleep(7000);
    	System.out.println("Control enters into the  When statement for Search file Transaction with Instance Id");
    	//ObjMLTComp.fileUploadPepsi();
    	ObjMLTComp.globalSearch(transaction, instanceid);
    	ObjMLTComp.transListPage(transaction,instanceid);
    	ObjMLTComp.logoutMLT();
    	System.out.println("Execution completed for When statement of Search file Transaction with Instance Id");
    
    }

    @Then("^Login the EC$")
    public void login_the_ec() throws Throwable {
    	
    	System.out.println("Execution Control Enters into Given statement for Login EC");
    	ObjMLTComp.loginEC();
		System.out.println("Execution Completed for Given Statement of EC ");
    	
    }
    
    
    @And("^Search the (.+) for the Employee$")
    public void search_the_for_the_employee(String ecportlet) throws Throwable
    {
    	System.out.println("Execution Control Enters into Search the Compensation Portlet for the Employee in  EC");
    	ObjMLTComp.CompensationPortletEC(ecportlet);
    	System.out.println("Execution Completed for Search the Compensation Portlet for the Employee in  EC ");
    	
    }

	
    
    
    }//Closing curly braces for the Class




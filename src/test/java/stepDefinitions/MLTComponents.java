package stepDefinitions;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import com.reusable.library.Base;
import com.reusable.library.PageObjectO2;

import config.ReadPropFile;
import junit.framework.Assert;

public class MLTComponents {
	
	public static WebDriver driver;
	PageObjectO2 pgObj = new PageObjectO2();
	
	//Variable Declaration
	public String EmplId_RT;
	public String EventDate_RT;
	public String StartDateTemp;
	public String PayCompValFile;
	public String PayComponent_RT;
	public String PayCompVal_RT;
	public String TransInstanceStatusRT;
	public String EventDate_EC;
	public String EventDateConEC;
	public String FreqCode_RT;
	
	HashMap<String,List<String>> TransDtlpage = new HashMap<String,List<String>>(); 
	
	
	
	/*******************Components of MLT Functionalities*********************/
	/*
	 * Component Name: loginMLT
	 * Purpose: To Login the MLT
	 * Author: Radha Manogari
	 * Created Date: 7/18/2020
	 * Last Modified Date: 7/18/2020
	 * Purpose of Modifying: 
	 * 
	 */
	
	public void loginMLT() 
	{
		try {
		System.out.println("Execution Control Enters into loginMLT Component");
		driver=Base.getDriver();
		ReadPropFile dataObj = new ReadPropFile();
		driver.get(dataObj.getMLTUrl());
		driver.findElement(By.xpath(pgObj.userNametxtbx_xpath)).sendKeys(dataObj.getMLTUserName());	
		driver.findElement(By.xpath(pgObj.pwdtxtbx_xpath)).sendKeys(dataObj.getMLTPwd());
		driver.findElement(By.xpath(pgObj.loginbtn_xpath)).click();
		Thread.sleep(3000);
		WebElement element = driver.findElement(By.xpath(pgObj.clntSlctnPepsi_xpath));
		Actions action = new Actions(driver);
		action.moveToElement(element).click().build().perform();
		Thread.sleep(5000);
		System.out.println("Execution Completed for loginMLT Component");
		
		}catch(Exception e)
		{
			String errormsg = e.getMessage();
			System.out.println("Error occured in the loginMLT Component: " + errormsg);
		}
	
		
	}
	
	/*
	 * Component Name: globalSearch
	 * Purpose: To search the instance through gloabl search
	 * Author: Radha Manogari
	 * Created Date: 7/18/2020
	 * Last Modified Date: 7/18/2020
	 * Purpose of Modifying: 
	 * 
	 */
	public void globalSearch(String transaction, String instanceid) 
	{
		try {
			System.out.println("Execution Control Enters into globalSearch Component");
			
			Thread.sleep(7000);
	    	System.out.println("Control enters into the  When statement for Search file Transaction  ");
	    	WebElement gblSearchtxtbx = driver.findElement(By.xpath(pgObj.globalSearchtxtbx_xpath));
			Actions action = new Actions(driver);
			action.moveToElement(gblSearchtxtbx).click();
			action.sendKeys(instanceid);
			action.build().perform();
			WebElement element = driver.findElement(By.xpath(pgObj.globalSearchicon_xpath));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			Thread.sleep(5000);
			driver.findElement(By.xpath(pgObj.glblExpand_xpath)).click();
			driver.findElement(By.xpath(pgObj.glblEditicon_xpath)).click();
			System.out.println("Execution Completed for globalSearch Component");
			Thread.sleep(4000);
			System.out.println("Execution Completed for globalSearch Component");
			
		}catch(Exception e)
		{
			String errormsg = e.getMessage();
			System.out.println("Error occured in the globalSearch Component: " + errormsg);
		}
	}
	
	/*
	 * Component Name: transListPage
	 * Purpose: To navigate to the TransList Page and check the Instance & Entities status
	 * Author: Radha Manogari
	 * Created Date: 7/18/2020
	 * Last Modified Date: 7/18/2020
	 * Purpose of Modifying: 
	 * 
	 */
	public void transListPage(String transaction,String instanceid) 
	{
		try {

			System.out.println("Execution Control Enters into transListPage Component");
			TransInstanceStatusRT = driver.findElement(By.xpath(pgObj.TransInstanceStatus_xpath)).getText();
			ArrayList<String> InstananceStatusRT = new ArrayList<String>(){{
		            add("File Received");
		            add("File Reformatted");
		            add("New / Awaiting validation");
		            add("Production Load in Process");
		            add("Queued up for validation");
		            add("Validation in Process");
		            add("Ready to Load");
		        }};
		        if(InstananceStatusRT.contains(TransInstanceStatusRT)){
		        	Thread.sleep(2000);
			    	driver.findElement(By.xpath(pgObj.RefreshiconTrans_xpath)).click();
		        }
		      //100062394 - Instance with more error entities 
		      //100014740 - BasePay Instance with more entities which are all in Validation Error
		      //100014936 - BasePay  Instance with more entities which has both Validation Error and Loaded Status
		      //100014738 - BasePay instance with 3 entities and in completely loaded status
		        ArrayList<String> InstErrorStatRT = new ArrayList<String>(){{
		        	add("Exceeded Error Threshold");
		        	add("Exceeded Trending Tolerances");
		        	add("Needs Client Attention");
		        	add("Needs Operations Attention");
		        }};
		        
		        if(InstErrorStatRT.contains(TransInstanceStatusRT)){
		        	Thread.sleep(2000);
		        	String TransErrorEntityCount ="(//div[@class='x-grid-item-container'])[2]/table/tbody/tr/td/div/form/select/option[1]";
		        	String TransLoadedEntityCount="(//div[@class='x-grid-item-container'])[2]/table/tbody/tr/td/div[text()='Loaded']";
		        	int Entity_Count = driver.findElements(By.xpath(pgObj.TransListEntitieListRow_xpath)).size();
		        	System.out.println("The No. of Entity are in Error Status: "+Entity_Count);
		        	int LoadedEntitiesCount = driver.findElements(By.xpath(TransLoadedEntityCount)).size();
		        	System.out.println("The No. of Entity are in Loaded Status: "+LoadedEntitiesCount);
			    	int ErrEntityCount = driver.findElements(By.xpath(pgObj.TransErrEntityStatusRT_xpath)).size();
			    	String TransEntityStatusRT = driver.findElement(By.xpath(pgObj.TransErrEntityStatusRT_xpath)).getText();
			    	System.out.println("The Uploaded File have some error(s) and the no. of. Enitites are in Error Status is: " + ErrEntityCount + "Entity status is: " + TransEntityStatusRT);
			    	ArrayList<String> ErrEntityNum = new ArrayList<String>();
			    	for(int i=1;i<=ErrEntityCount;i++)
			    	{
			    	 String first_part = "//div[@id='entityGrid-body']/div/descendant::table[";
					 String second_part = "]/descendant::option[text()='Validation Error']/following::sup[text()='Edit']/ancestor::div[1]";
					 String third_part ="/a/sup[text()='Edit']";
					 String FinalErrEntity = first_part+i+second_part;
					 String ErrEntityNumtxt = driver.findElement(By.xpath(FinalErrEntity)).getText();
					 System.out.println("The entity number is: "+ErrEntityNumtxt );
					 String[] ErrorEntityNumtxt = ErrEntityNumtxt.split(" ");
					 String ErrrEntityNum =ErrorEntityNumtxt[0];
					 System.out.println("The Entity which has Validation Error: "+ ErrrEntityNum);
					 ErrEntityNum.add(ErrrEntityNum);
					 System.out.println("The List of Entities are in Loaded Status Status: "+ ErrEntityNum);
					 String TransEdit_xpath = first_part+i+second_part+third_part;
					 driver.findElement(By.xpath(TransEdit_xpath)).click();
					 Thread.sleep(2000);
					 int Row_count = driver.findElements(By.xpath(pgObj.TransDtlCrntErrorsRow_xpath)).size();
					 System.out.println("No.of.Rows in Current Error Section  Table: " + Row_count );
					 int Col_count= driver.findElements(By.xpath(pgObj.TransDtlCrntErrorsCol_xpath)).size();
					 System.out.println("No.of.Columns in the Current Error Section: " + Col_count );
					 ArrayList<String> ErroMsgs = new ArrayList<String>();
					 for (int j=1;j<=Row_count;j++){
					 String RTErrormsg = driver.findElement(By.xpath(pgObj.TransDtlCrntErrorMsg_xpath)).getText();
					 System.out.println("The Entity has following error message: "+ RTErrormsg );
					 ErroMsgs.add(RTErrormsg);
					 System.out.println("The following are the Error message displaying for the Enitty : "+ ErroMsgs);
			    	 //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(pgObj.TransDtlCurrntErrorHdr_xpath)));
					 }
					 driver.findElement(By.xpath(pgObj.PepTransDtBackbtn_xpath)).click();
					 Thread.sleep(2000);
		        }
		        }
		        ArrayList<String> FatalInstErrorStatRT = new ArrayList<String>(){{
		        	add("Duplicate File");
		        	add("Empty File");
		        	add("IT Errors");
		        	add("Inactive File Definition");
		        	add("Not Ready to Load");
		        	add("Rejected by System");
		        }};
		        if(FatalInstErrorStatRT.contains(TransInstanceStatusRT)) //need to work - 100083187 -in HIG(use this instance)
		        {
		        	Thread.sleep(2000);
			    	driver.findElement(By.xpath(pgObj.RefreshiconTrans_xpath)).click();
			    	String TransEntityStatusRT = driver.findElement(By.xpath(pgObj.TransErrEntityStatusRT_xpath)).getText();
			    	System.out.println("The Uploaded File have some error(s) and the Entity status is: " + TransEntityStatusRT );
			    	driver.findElement(By.xpath(pgObj.TransListEditLink)).click();
			    	Thread.sleep(5000);
			    	//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(pgObj.TransDtlCurrntErrorHdr_xpath)));
			    	driver.findElement(By.xpath(pgObj.TransDtlCurrntErrorHdr_xpath)).click();
			    	int Row_count = driver.findElements(By.xpath(pgObj.TransDtlCrntErrorsRow_xpath)).size();
					System.out.println("No.of.Rows in Current Error Section  Table " + Row_count );
					int Col_count= driver.findElements(By.xpath(pgObj.TransDtlCrntErrorsCol_xpath)).size();
					System.out.println("No.of.Columns in Current Error Section " + Col_count );
					
					ArrayList<String> ErroMsgs = new ArrayList<String>();
					for (int i=1;i<=Row_count;i++){
						
					String RTErrormsg = driver.findElement(By.xpath(pgObj.TransDtlCrntErrorMsg_xpath)).getText();
					System.out.println("The Instance has following error message: "+ RTErrormsg );
					ErroMsgs.add(RTErrormsg);
					System.out.println("The following are the Error displaying for the Instance ID: "+ ErroMsgs);
					}
		        }	
		
		        if(TransInstanceStatusRT.equals("Validation Successful"))//Validation Successful
		    	{
		    		Thread.sleep(2000);
		    		System.out.println("The file reached Validation Successful and can be load to Workday: " + TransInstanceStatusRT );
		    		driver.findElement(By.xpath(pgObj.LoadToWorkdaybtn_xpath)).click();
		    		//Load to Workday Dialogbox
		    		if(driver.findElement(By.xpath(pgObj.LoadToWorkdayDialogbx_xpath)).isDisplayed())
		    		{
		    			driver.findElement(By.xpath(pgObj.LoadToWorkdaydialogYes_xpath)).click();
		    		}
		    		Thread.sleep(60000);
		    		String InstanceCmptlyLooadedRT = driver.findElement(By.xpath(pgObj.TransInstanceStatus_xpath)).getText();
		    		if(InstanceCmptlyLooadedRT.equals("Completely Loaded"))
		    		{
		    			System.out.println("The Uploaded File Loaded into Workday and the status of the Instance is: " + InstanceCmptlyLooadedRT );
		    			String LoadedEntityStatusRT = driver.findElement(By.xpath(pgObj.TransErrEntityStatusRT_xpath)).getText();
		    			if(LoadedEntityStatusRT.equals("Loaded"))
		    			{
		    				driver.findElement(By.xpath(pgObj.DetailsLnkLoadedEntity_xpath)).click();
		    				Thread.sleep(20000);
		    				//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(pgObj.TransDetailHeadr_xpath)));
		    				driver.findElement(By.xpath(pgObj.TransDtlShowAllChbx_xpath)).click();
		    				//WID=driver.findElement(By.xpath(pgObj.TransDetlWID_xpath)).getText();
		    			}
		    		}	
		    	}  
		        else if(TransInstanceStatusRT.equals("Completely Loaded"))
		    	{
		        	System.out.println("The Uploaded File Loaded into EC Successfully since the status of the Instance is: " + TransInstanceStatusRT);
		        	transDtlCompletelyLoadedInst(transaction,instanceid);
		    
		    	}
			
		}catch(Exception e)
		{
			String errormsg = e.getMessage();
			System.out.println("Error occured in the transListPage Component: " + errormsg);
		}
		
	}
	
	 /* 
	 * Component Name: logoutMLT
	 * Purpose: To logut from MLT and closing the browser
	 * Author: Radha Manogari
	 * Created Date: 7/18/2020
	 * Last Modified Date: 7/18/2020
	 * Purpose of Modifying: 
	 */
	public void logoutMLT()
	{
		try {
			System.out.println("Execution Control Enters into logoutMLT Component");
			WebElement Userprofile = driver.findElement(By.xpath(pgObj.Userprobtn_xpath));
			Actions action1 = new Actions(driver);
			action1.moveToElement(Userprofile).build().perform();
			Thread.sleep(2000);
			WebElement logout = driver.findElement(By.xpath(pgObj.logout_xpath));
			action1.moveToElement(logout).click().build().perform();
			Thread.sleep(3000);
			//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(pgObj.logoutdialogbox_xpath)));
			driver.findElement(By.xpath(pgObj.logoutdialogbxYes_xpath)).click();
			Thread.sleep(2000); 
			driver.quit();
			System.out.println("Execution Completed for logoutMLT Component");
			
		}catch(Exception e)
		{
			String errormsg = e.getMessage();
			System.out.println("Error occured in the logoutMLT Component: " + errormsg);
		}
	}
	
	/*
	 * Component Name: loginEC
	 * Purpose: To Login the EC application
	 * Author: Radha Manogari
	 * Created Date: 7/18/2020
	 * Last Modified Date: 7/18/2020
	 * Purpose of Modifying: 
	 */
	public void loginEC() 
	{
		try {
			System.out.println("Execution Control Enters into loginEC Component");
			driver=Base.getDriver();
			driver.manage().window().maximize();
			ReadPropFile dataObj = new ReadPropFile();
			Actions action = new Actions(driver);
			driver.get(dataObj.getECURL());
			driver.findElement(By.xpath(pgObj.ECEnterCompanyIdtxtbx_xpath)).sendKeys(dataObj.ECCompanyID());
			driver.findElement(By.xpath(pgObj.ECCompanyIdSubmitbtn_xpath)).click();
			Thread.sleep(2000); // need to increase the sleep time during execution
			driver.findElement(By.name(pgObj.ECUserNametxt_name)).sendKeys(dataObj.ECUserName());
			driver.findElement(By.name(pgObj.ECPasswrdtxt_name)).sendKeys(dataObj.ECPwd());
			Thread.sleep(1000);
			WebElement loginbtn = driver.findElement(By.xpath(pgObj.ECLoginbtn_xpath));
			action.moveToElement(loginbtn).click().build().perform();	
			Thread.sleep(1000);
			System.out.println("Execution Completed for loginEC Component");
			
		}catch(Exception e)
		{
			String errormsg = e.getMessage();
			System.out.println("Error occured in the loginEC Component: " + errormsg);
		}
	}
	
	/*
	 * Component Name: CompensationPortletEC
	 * Purpose: To view the Compensation Portlet of an Employee through Employee Id
	 * Author: Radha Manogari
	 * Created Date: 7/18/2020
	 * Last Modified Date: 7/18/2020
	 * Purpose of Modifying: 
	 */
	public void CompensationPortletEC(String ecportlet) 
	{
      try {
    	  
    	  System.out.println("Execution Control Enters into CompensationPortletEC Component");
    	  System.out.println("The portlet from feature file is: " + ecportlet);
    	  Actions action = new Actions(driver);
    	  driver.findElement(By.xpath(pgObj.ECSearchtxtbx_xpath)).sendKeys(EmplId_RT);
    	  Thread.sleep(1000);
    	  driver.findElement(By.xpath(pgObj.ECSearchtxtbx_xpath)).sendKeys(Keys.ARROW_DOWN);
    	  driver.findElement(By.xpath(pgObj.ECSearchtxtbx_xpath)).sendKeys(Keys.ENTER);
    	  driver.findElement(By.xpath(pgObj.ECJobInfoheader_xpath)).click();
    	  WebElement element = driver.findElement(By.xpath(pgObj.ECEmployeeExpandicon_xpath));
    	  JavascriptExecutor js = (JavascriptExecutor)driver;   
    	  js.executeScript("arguments[0].setAttribute('style','background: yellow; border: 2px solid red;');", element);
    	  action.moveToElement(element).click().build().perform();
    	  WebElement ECPortletsoptions = driver.findElement(By.xpath(pgObj.ECPortletslistbx_xpath));
    	  List<WebElement> ECPortletSelect = ECPortletsoptions.findElements(By.xpath(pgObj.ECPortletCompInfo_xpath));
    	  for (WebElement ECPortletRO : ECPortletSelect)
    	  {
    		  if (ECPortletRO.getText().equals(ecportlet))
    		  {
    			  ECPortletRO.click(); 
    			  break;
    		  }
    	  }
    	  Thread.sleep(1000);
    	  driver.findElement(By.xpath(pgObj.ECCompInfoHistorybtn_xpath)).click();
    	  StartDateTemp = "Mar 07, 2020"; // need to modify by getting from MLT TransDetail page & to find the method to convert the 03/07/2020 into Mar 07, 2020
    	  PayCompValFile = "122,100.00";  // need to modify by getting from MLT TransDetail page
    	  List<WebElement> ECRecordCount = driver.findElements(By.xpath(pgObj.ECRecordEventDateList_xpath));
    	  for(WebElement ECRecordToprow :ECRecordCount)
    	  {
    		  if(ECRecordToprow.getText().equals(StartDateTemp))
    		  {
    			  driver.findElement(By.xpath(pgObj.ECReocrdEventDate_xpath)).click();
    			  Thread.sleep(3000);
    			  //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(pgObj.ECEventReasonlbl_xpath)));
    			  String ECPaycompValue = driver.findElement(By.xpath(pgObj.ECPayCompValue_xpath)).getText();
    			  if(PayCompValFile.contains(ECPaycompValue))
    			  {
    				  Assert.assertTrue("Base Salary updated", true);
    			  }
    			  break;
    		  }

    	  }
      }catch(Exception e)
      {
    	  String errormsg = e.getMessage();
    	  System.out.println("Error occured in the CompensationPortletEC Component: " + errormsg);
      }
	}


	/*
	 * Component Name: fileUploadPepsi
	 * Purpose: To upload the file in MLT for Pepsi Client
	 * Author: Radha Manogari
	 * Created Date: 7/18/2020
	 * Last Modified Date: 7/18/2020
	 * Purpose of Modifying: 
	 */
public void fileUploadPepsi()
{
	try {
		    System.out.println("Execution Control Enters into fileUploadPepsi Component");
		    Thread.sleep(4000);
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("document.getElementById('mltFileUploadButton-btnIconEl').click();");
			Thread.sleep(7000);
			if(driver.findElement(By.id(pgObj.Headertxtfileupload_id)).isDisplayed()) {
				Pattern SelectFile = new Pattern("C:\\20056283\\Selenium\\SikuliImage\\SelectFile.PNG");
				Pattern FilePath = new Pattern("C:\\20056283\\Selenium\\SikuliImage\\FilePath.PNG");
				Pattern Openbtn = new Pattern("C:\\20056283\\Selenium\\SikuliImage\\Openbtn.PNG");
				
				Screen src = new Screen();
				src.setAutoWaitTimeout(30);
				src.doubleClick(SelectFile);
				Thread.sleep(5000);
				src.type(FilePath, "C:\\Users\\A1015238\\Downloads\\BaseAndRecurringPay.xls");
				src.click(Openbtn);
				
				Thread.sleep(9000);
				System.out.println("File Uploaded successfully through Sikuli");
				Thread.sleep(8000);
				int Confirmationdialogbx = driver.findElements(By.xpath(pgObj.ConfirmInstance_xpath)).size();
	        	System.out.println("The Confirmation dialog box for navigating to Translist page  : "+ Confirmationdialogbx);
				
	        	if(Confirmationdialogbx>0)
	        	{
	              int yesbtninstance = driver.findElements(By.xpath(pgObj.InstanceToTranspage_xpath)).size();
	              System.out.println("The Yes button to navigate to TransList Page is available : "+ yesbtninstance);
				  driver.findElement(By.xpath(pgObj.InstanceToTranspage_xpath)).click();
				  Thread.sleep(7000);
	        	}
				System.out.println("Successfully navigated to TransList page");
			}
			else
			{
				System.out.println("Not able to upload the file through Sikuli");
			}
		 
		   System.out.println("Execution Completed for fileUploadPepsi Component");
		
	     }catch(Exception e)
          {
	    	 String errormsg = e.getMessage();
	    	 System.out.println("Error occured in the fileUploadPepsi Component: " + errormsg);
         }
   }
	
/*
 * Component Name: fileUploadPepsi
 * Purpose: To upload the file in MLT for Pepsi Client
 * Author: Radha Manogari
 * Created Date: 7/18/2020
 * Last Modified Date: 7/18/2020
 * Purpose of Modifying: 
 */
	public void transDtlCompletelyLoadedInst(String transaction,String instanceid)
	{
		try {
			System.out.println("Execution Control Enters into completelyLoadedInstance Component");
    		int Entity_Count = driver.findElements(By.xpath(pgObj.TransListEntitieListRow_xpath)).size();
    		int Loaded_Count = driver.findElements(By.xpath(pgObj.Loaded_xpath)).size();
			System.out.println("The number of Entities in the Instance is/are: " + Entity_Count + " and the status of the Entities is/are:  " + Loaded_Count );
			
			ArrayList<String> LoadedTransNum = new ArrayList<String>();
			for(int i=1;i<=Entity_Count;i++)
			{
				 Thread.sleep(5000);
				 String first_part = "(//div[@class='x-grid-item-container'])[2]/table[";
				 String second_part = "]/tbody/tr/td/div[text()='Loaded']/following::td/div";
				 String third_part="/a/sup[text()='Details']";
				 String FinalLoadedTrans = first_part+i+second_part;
				 String LoadedTransNumtxt = driver.findElement(By.xpath(FinalLoadedTrans)).getText();
				
				 String[] LoadedTransNumspliting = LoadedTransNumtxt.split(" ");
				 String LoadedEntityName =LoadedTransNumspliting[0];
				 System.out.println("The Entity which is Loaded: "+ LoadedEntityName);
				 
				 String DetailsLink_xpath = first_part+i+second_part+third_part;
				 driver.findElement(By.xpath(DetailsLink_xpath)).click();
	    		 Thread.sleep(5000);
	    		 
	    		 String TransactionName = transaction;
	    		 if(TransactionName.equalsIgnoreCase("BasePay"))
	    		 {
	    			 BasePayTransaction();
	    		 }
	    		 else
	    		 {
	    			 System.out.println("The Transaction Name is not found to Navigate to the Transaction Detail page");
	    		 }
				 LoadedTransNum.add(LoadedEntityName);
				 driver.findElement(By.xpath(pgObj.PepTransDtBackbtn_xpath)).click();
				 Thread.sleep(3000);
				 
				 System.out.println("The List of Entities are in Loaded Status Status: "+ LoadedTransNum);
			}
    		     System.out.println("Execution Completed for completelyLoadedInstance Component");
			
		}catch(Exception e)
        {
	    	 String errormsg = e.getMessage();
	    	 System.out.println("Error occured in the completelyLoadedInstance Component: " + errormsg);
       }
	}
	
	/*
	 * Component Name: dateConversionEC
	 * Purpose: To convert the date from the format MM/dd//yyyy to MMM dd, yyyy
	 * Author: Radha Manogari
	 * Created Date: 7/18/2020
	 * Last Modified Date: 7/18/2020
	 * Purpose of Modifying: 
	 */
	 public  String dateconvertEC(String dateString) throws ParseException 
	 {
		  DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy", Locale.US); 
	      Date date1 = format1.parse(dateString);
	      System.out.println("String converted to Dateformat : " + new SimpleDateFormat("MM/dd/yyyy").format(date1));
	      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy");
	      EventDate_EC = simpleDateFormat.format(date1);
	      return EventDate_EC;
	 }
	
	 /*
	 * Component Name: BasePayTransaction
	 * Purpose: To view the Trans Detail page of the Completely loaded BasePay Transaction
	 * Author: Radha Manogari
	 * Created Date: 7/18/2020
	 * Last Modified Date: 7/18/2020
	 * Purpose of Modifying: 
	 */
	
	 public void BasePayTransaction()
	 {
		 try {
			 System.out.println("Execution Control Enters into BasePayTransaction Component");
			 EmplId_RT = driver.findElement(By.xpath(pgObj.PepTransDtEmpId_xpath)).getText();
    		 EventDate_RT = driver.findElement(By.xpath(pgObj.PepTransDtEventDate_xpath)).getText();
    		 EventDateConEC = dateconvertEC(EventDate_RT);
    		 PayComponent_RT= driver.findElement(By.xpath(pgObj.PepTransDtPayComp)).getText();
    		 PayCompVal_RT =driver.findElement(By.xpath(pgObj.PepTransDtPayCompVal)).getText();
    		 FreqCode_RT=driver.findElement(By.xpath(pgObj.PepTransDtFreqCode)).getText();
    		 
    		 TransDtlpage.put(EmplId_RT, new ArrayList<String>()); 
    		 TransDtlpage.get(EmplId_RT).add(PayComponent_RT);
    		 TransDtlpage.get(EmplId_RT).add(PayCompVal_RT);
    		 TransDtlpage.get(EmplId_RT).add(FreqCode_RT);
    		 TransDtlpage.get(EmplId_RT).add(EventDateConEC);
			 
    		 System.out.println(TransDtlpage);
			 System.out.println("Execution Completed for BasePayTransaction Component");
		 }catch(Exception e)
		 {
			 String errormsg = e.getMessage();
	    	 System.out.println("Error occured in the BasePayTransaction Component: " + errormsg);
		 }
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	

} //Closing curly braces for Class

package com.reusable.library;

public class PractisingCodes{
	
	
	/*
	 * Code No: 07_04_2020_01
	 * Code from Step Definition - Altering the Entity Check into Instance check
	 * Need to copy paste the code if the entity reacheds Validation error
	 * Date : 07/04/2020
	 * 
	 * 	if(driver.findElements(By.xpath(pgObj.TransListEntityList_xpath)).size()>0)
		{
			int Entity_Count = driver.findElements(By.xpath(pgObj.TransListEntitieListRow_xpath)).size();
			System.out.println("The number of Entity in the Instance is: " + Entity_Count);
			for(int i=1;i<=Entity_Count;i++)
			{
				
				if(driver.findElements(By.xpath(pgObj.ValidationErrorRow_xpath)).size()>0)
				{
					int ValidationError_Count = driver.findElements(By.xpath(pgObj.ValidationErrorRow_xpath)).size();
					System.out.println("The number of Entities reached Validation Error " + ValidationError_Count);
					 String first_part = "(//div[@class='x-grid-item-container'])[2]/table/tbody/tr[";
					 String second_part = "]/td[";
					 String third_part = "]/div/form/select/option[text()='Validation Error']";
					 
					ArrayList<String> ErrorTransNum = new ArrayList<String>();
					for(int j=1;j<=ValidationError_Count;j++)
					{
					    String FinalFailedTransNum = first_part+i+second_part+j+third_part;
						String FailedTransNum = driver.findElement(By.xpath(FinalFailedTransNum)).getText();
						
						System.out.println("The Transaction which has validation Error: "+ FailedTransNum);
						ErrorTransNum.add(FailedTransNum);
					}
					
					System.out.println("The List of Transactions are in Validation Error Status: "+ ErrorTransNum);
				}	
				
				
				else if(driver.findElements(By.xpath(pgObj.Loaded_xpath)).size()>0)
            {
                
				int Loaded_Count = driver.findElements(By.xpath(pgObj.Loaded_xpath)).size();
				System.out.println("The number of Entities reached Loaded Status is/are: " + Loaded_Count);
				 //String first_part = "(//div[@class='x-grid-item-container'])[2]/table/tbody/tr[";
				 //String second_part = "]/td[";
				 //String third_part = "]/div[text()='Loaded']/following::td/div";
				//String FinalLoadedTrans = first_part+i+second_part+j+third_part;
				 
				 String first_part = "(//div[@class='x-grid-item-container'])[2]/table[";
				 String second_part = "]/tbody/tr/td/div[text()='Loaded']/following::td/div";
				 
					ArrayList<String> LoadedTransNum = new ArrayList<String>();
					//for(int j=1;j<=Loaded_Count;j++)
					//{
						//(//div[@class='x-grid-item-container'])[2]/table[3]/tbody/tr/td/div[text()='Loaded']/following::td/div
						
						String FinalLoadedTrans = first_part+i+second_part;
						String LoadedTransNumtxt = driver.findElement(By.xpath(FinalLoadedTrans)).getText();
						
						//String FinalLoadedTrans = first_part+i+second_part+j+third_part;
						//String LoadedTransNumtxt = driver.findElement(By.xpath(FinalLoadedTrans)).getText();
						System.out.println("The Entity which is loaded: "+ LoadedTransNumtxt);
						LoadedTransNum.add(LoadedTransNumtxt);
					//}
					System.out.println("The List of Entities are in Loaded Status Status: "+ LoadedTransNum);
			 }
				
				
			else if(driver.findElements(By.xpath(pgObj.ValidationErrorRow_xpath)).size()>0)
				{
					int ValidationError_Count = driver.findElements(By.xpath(pgObj.ValidationErrorRow_xpath)).size();
					System.out.println("The number of Entities reached Validation Error " + ValidationError_Count);
					 String first_part = "(//div[@class='x-grid-item-container'])[2]/table/tbody/tr[";
					 String second_part = "]/td[";
					 String third_part = "]/div/form/select/option[text()='Validation Error']";
					 
					ArrayList<String> ErrorTransNum = new ArrayList<String>();
					for(int j=1;j<=ValidationError_Count;j++)
					{
					    String FinalFailedTransNum = first_part+i+second_part+j+third_part;
						String FailedTransNum = driver.findElement(By.xpath(FinalFailedTransNum)).getText();
						
						System.out.println("The Transaction which has validation Error: "+ FailedTransNum);
						ErrorTransNum.add(FailedTransNum);
					}
					
					System.out.println("The List of Transactions are in Validation Error Status: "+ ErrorTransNum);
				}
				else if(driver.findElements(By.xpath(pgObj.PreValidatFailedRow_xpath)).size()>0)
				 {
					 int PreValFailed_Count = driver.findElements(By.xpath(pgObj.PreValidatFailedRow_xpath)).size();
					 System.out.println("The number of Entities reached Pre-ValidationSuccessful " + PreValFailed_Count);
					 String first_part = "(//div[@class='x-grid-item-container'])[2]/table/tbody/tr[";
					 String second_part = "]/td[";
					 String third_part = "]/div/form/select/option[text()='Pre-validation Failed']";
						
						ArrayList<String> PreValFailedTransNum = new ArrayList<String>();
						for(int j=1;j<=PreValFailed_Count;j++)
						{
							String FinalPreValFailTrans = first_part+i+second_part+j+third_part;
							String PreValiFailTransNum = driver.findElement(By.xpath(FinalPreValFailTrans)).getText();
							
							System.out.println("The Transaction which has validation Error: "+ PreValiFailTransNum);
							PreValFailedTransNum.add(PreValiFailTransNum);
						}
						
						System.out.println("The List of Transactions are in Validation Error Status: "+ PreValFailedTransNum);
				 }
				else if(driver.findElements(By.xpath(pgObj.PreValidatSuccessfulRow_xpath)).size()>0)
				 {
					 int PreValSuccessful_Count = driver.findElements(By.xpath(pgObj.PreValidatSuccessfulRow_xpath)).size();
					 
					 System.out.println("The number of Entities reached Pre-ValidationSuccessful " + PreValSuccessful_Count);
					 String first_part = "(//div[@class='x-grid-item-container'])[2]/table/tbody/tr[";
					 String second_part = "]/td[";
					 String third_part = "]/div[text()='Pre-validation Successful']";
						ArrayList<String> PreValSuccessfulTransNum = new ArrayList<String>();
						for(int j=1;j<=PreValSuccessful_Count;j++)
						{
							String FinalPreValSucfulTrans = first_part+i+second_part+j+third_part;
							String PreValiSuccessTransNum = driver.findElement(By.xpath(FinalPreValSucfulTrans)).getText();
							System.out.println("The Transaction which has validation Error: "+ PreValiSuccessTransNum);
							PreValSuccessfulTransNum.add(PreValiSuccessTransNum);
						}
						System.out.println("The List of Transactions are in Validation Error Status: "+ PreValSuccessfulTransNum);
				 }
				
				}
		}
		
       
       **************************End for  07_04_2020_01*********************************************************
        
	 */
	
	
	}
	
	
	
	

	



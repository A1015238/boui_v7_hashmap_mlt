Feature: BOUI Pepsi QA

@MLTLogin
Scenario Outline: Global Search the Instance Id
Given User in the Home Page
When Search the file for  <Transaction> with the <InstanceId>
#Then Login the EC
#And Search the <ECPortlet> for the Employee 

Examples:
|Transaction|InstanceId|ECPortlet|
|BasePay    |100014738|Compensation Information|

#//100062394 - Instance with more error entities 
#//100014740 - BasePay Instance with more entities which are all in Validation Error
#//100014936 - BasePay  Instance with more entities which has both Validation Error and Loaded Status
#//100014738 - BasePay instance with 3 entities and in completely loaded status


# System.out.println("Execution Control Enters into BasePayTransaction Component");
# System.out.println("Execution Completed for completelyLoadedInstance Component");